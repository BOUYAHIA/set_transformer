"""Defines the neural network, losss function and metrics"""

import torch
import torch.nn as nn
import torch.nn.functional as F

class InvariantModel(nn.Module):
    def __init__(self, params, phi: nn.Module, rho: nn.Module):
        super().__init__()
        self.phi = phi
        self.rho = rho
        self.params = params

    def forward(self, x, sizes_batch):
        nb_exemple = x.shape[0]
        x = self.phi.forward(x)

        x = torch.sum(x, dim=1)
        #x = torch.sum(x, dim=1, keepdim=True)

        # compute the output
        out = self.rho.forward(x)

        out = out.reshape(nb_exemple)

        return out


class Phi(nn.Module):
    def __init__(self, params):
        super().__init__()
        self.params = params

        self.fc1 = nn.Linear(self.params.input_size, self.params.hidden_dim, bias=False)
        self.bn1 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc2 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim, bias=False)
        self.bn2 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc3 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim, bias=False)
        self.bn3 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc4 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim, bias=False)
        self.bn4 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc5 = nn.Linear(self.params.hidden_dim, self.params.projection_dim, bias=False)

    def forward(self, x):
        """
        out = F.relu(self.fc1(x))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = F.relu(self.fc4(out))

        out = F.relu(self.bn1(self.fc1(x)))
        out = F.relu(self.bn2(self.fc2(out)))
        out = F.relu(self.bn3(self.fc3(out)))
        out = F.relu(self.bn4(self.fc4(out)))
        """
        out = self.fc1(x)
        out = torch.transpose(out, 1, 2)
        out = F.relu(self.bn1(out))
        out = torch.transpose(out, 1, 2)

        out = self.fc2(out)
        out = torch.transpose(out, 1, 2)
        out = F.relu(self.bn2(out))
        out = torch.transpose(out, 1, 2)

        out = self.fc3(out)
        out = torch.transpose(out, 1, 2)
        out = F.relu(self.bn3(out))
        out = torch.transpose(out, 1, 2)

        out = self.fc4(out)
        out = torch.transpose(out, 1, 2)
        out = F.relu(self.bn4(out))
        out = torch.transpose(out, 1, 2)

        out = self.fc5(out)

        return out


class Rho(nn.Module):
    def __init__(self, params):
        super().__init__()
        self.params = params
        self.bn6 = nn.BatchNorm1d(num_features=self.params.projection_dim)
        self.fc6 = nn.Linear(self.params.projection_dim, self.params.hidden_dim)
        self.fc7 = nn.Linear(self.params.hidden_dim, self.params.output_size)

    def forward(self, x):
        x = self.bn6(x)
        out = F.relu(self.fc6(x))
        #out = self.fc7(out)
        out = torch.sigmoid(self.fc7(out))

        return out