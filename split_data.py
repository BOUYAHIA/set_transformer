import json
import os
import sys
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import ast
from sklearn import preprocessing
import pickle
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', default='supervised_data/dentaire_clause', help='Directory containing the dataset')
parser.add_argument('--train', default=0.7, help='Pourcentage train')
parser.add_argument('--val', default=0.15, help="Pourcentage validation")

def load_dataset(path_csv):
    """
    Loads dataset into memory from csv file
    Args :
        path_csv (string) : chemin du fichier qui contient les données
    :return
        X : les sets qui sont les formulaires
        Y : les étiquettes associées a ces formaulaires
    """
    data = pd.read_csv(path_csv, delimiter=';')

    X = data["data"]
    Y = data["IS_AUDITE"]

    return X, Y


def save_dataset(dataset, labels, save_dir):
    """
    Enregister dataset dans un fichier sets.txt and  et labels dans labels.txt dans le répertoire save_dir save_dir

    Args:
        dataset: [[0,1...., 1], [0,1...., 0], [0,0...., 1]]
        labels : 0
        save_dir: (string)
    """
    # Create directory if it doesn't exist
    print("Saving in {}...".format(save_dir))
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    # Export the dataset

    with open(os.path.join(save_dir, 'sets.txt'), 'w') as file_formulaires:
        with open(os.path.join(save_dir, 'labels.txt'), 'w') as file_labels:
            for i in range (len(labels)):
                file_formulaires.write(dataset[i].replace("], [", "];[")[1:-1]+"\n")
                file_labels.write(str(labels[i])+"\n")

    print("- done.")

def save_formatted_dataset(dataset, labels, save_dir):
    """
    Enregister dataset dans un fichier sets.txt et labels dans labels.txt dans le répertoire save_dir

    Args:
        dataset: [[0,1...., 1], [0,1...., 0], [0,0...., 1]]
        labels : 0
        save_dir: (string)
    """
    # Create directory if it doesn't exist
    print("Saving in {}...".format(save_dir))
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    # Export the dataset

    with open(os.path.join(save_dir, 'sets.txt'), 'w') as file_formulaires:
        with open(os.path.join(save_dir, 'labels.txt'), 'w') as file_labels:
            for i in range (len(labels)):
                file_formulaires.write(str(dataset[i]).replace("], [", "];[")[1:-1]+"\n")
                file_labels.write(str(labels[i])+"\n")

    print("- done.")

def list_of_set_to_array(data) :
    """
    fonction permettant de transformer une liste de sets en un numpy array
    :param data: liste of sets
    :return: a numpy array
    """

    list_data = list()
    data = data.tolist()

    for i in range(len(data)) :
        y = data[i].replace("], [", "];[")[1:-1].split(';')
        for j in range(len(y)) :
            list_data.append(ast.literal_eval(y[j]))

    return np.array(list_data)


def MinMaxScaler(data) :
    """
    appliquer MinMaxScaler sur un jeu de données en entrée et enregistrer l'objet de transformation dans un fichier 'scaler.pkl'
    :param data: liste de sets
    """

    data = list_of_set_to_array(data)
    min_max_scaler = preprocessing.MinMaxScaler()
    sc = min_max_scaler.fit(data)

    path_file = os.path.join(args.data_dir, 'scaler.pkl')

    pickle.dump(sc, open(path_file, 'wb'))

def transform(X_data, scaler) :
    X_data = X_data.tolist()
    list_data = list()

    for i in range(len(X_data)) :
        formulaire = X_data[i].replace("], [", "];[")[1:-1].split(';')
        formulaire_scaler = list()
        for j in range(len(formulaire)) :
            reclam = ast.literal_eval(formulaire[j])
            reclam = scaler.transform(np.array([reclam]))
            reclam = reclam.tolist()[0]
            formulaire_scaler.append(reclam)

        list_data.append(formulaire_scaler)

    return list_data

def save_dict_to_json(d, json_path):
    """Saves dict to json file

    Args:
        d: (dict)
        json_path: (string) path to json file
    """
    with open(json_path, 'w') as f:
        d = {k: v for k, v in d.items()}
        json.dump(d, f, indent=4)

if __name__ == '__main__':
    # Check that the dataset exists
    args = parser.parse_args()

    path_dataset = os.path.join(args.data_dir, 'data.csv')
    msg = "{} file not found. Make sure you have the right dataset".format(path_dataset)
    assert os.path.isfile(path_dataset), msg

    # Load the dataset into memory
    print("Loading dataset into memory...")
    X, Y = load_dataset(path_dataset)
    print("- done.")

    train_ratio = args.train
    validation_ratio = args.val
    test_ratio = 1 - (train_ratio + validation_ratio)

    # train is now 70% of the entire data set
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=1 - train_ratio, random_state=42)


    # test is now 15% of the initial data set
    # validation is now 15% of the initial data set
    X_val, X_test, y_val, y_test = train_test_split(X_test, y_test,
                                                    test_size=test_ratio / (test_ratio + validation_ratio),
                                                    random_state=42)

    MinMaxScaler(X_train)
    path_file = os.path.join(args.data_dir, 'scaler.pkl')
    scaler = pickle.load(open(path_file, 'rb'))

    X_train = transform(X_train, scaler)
    X_val = transform(X_val, scaler)
    X_test = transform(X_test, scaler)

    save_formatted_dataset(X_train, y_train.tolist(), os.path.join(args.data_dir, 'train'))
    save_formatted_dataset(X_val, y_val.tolist(), os.path.join(args.data_dir, 'val'))
    save_formatted_dataset(X_test, y_test.tolist(), os.path.join(args.data_dir, 'test'))

    input_size = len(X_test[0][0])

    #input_size = len(ast.literal_eval(X_test[0])[0])

    # Save datasets properties in json file
    sizes = {
        'input_size': input_size,
        'train_size': len(y_train),
        'dev_size': len(y_val),
        'test_size': len(y_test),
        'train_IS_AUDITE': sum(y_train),
        'dev_IS_AUDITE': sum(y_val),
        'test_IS_AUDITE': sum(y_test),
    }
    save_dict_to_json(sizes, os.path.join(args.data_dir, 'dataset_params.json'))







    
    
