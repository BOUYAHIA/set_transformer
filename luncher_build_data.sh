#!/bin/bash
path_certi="/home/guests/abdelali.bouyahia/sample_01102020.csv"
path_fourni="/data/UL/transfert/Prevention_de_la_fraude/GSS_FILE_FOURNISSEURS_20190610.csv"
path_contra="/data/UL/transfert/Prevention_de_la_fraude/Livraison_fraude_20210212/DonneesContractuelles.csv"
save_dir="supervised_data"
#CATG_RECLAM="autre_clause"
#CATG_RECLAM="soin_dentaire"
CATG_RECLAM="dentaire_clause"
log_file="${save_dir}/${CATG_RECLAM}/build_data.log"
rm -f "$log_file"
python build_data.py $path_certi $path_fourni $path_contra $save_dir $CATG_RECLAM