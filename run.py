"""Train the model"""
import argparse
import os

from data_loader import DataLoader
from tqdm import trange
from models import SetTransformer, DeepSet, SetTransformer_dropout
from models import weights_init_kaiming_uniform
from plot import plot, score
import utils

import numpy as np
import torch
import torch.nn as nn

parser = argparse.ArgumentParser()
parser.add_argument("--num_pts", type=int, default=10)
parser.add_argument("--learning_rate", type=float, default=1e-3)
parser.add_argument("--batch_size", type=int, default=32)
parser.add_argument("--dim", type=int, default=256)
parser.add_argument("--n_heads", type=int, default=4)
parser.add_argument("--n_anc", type=int, default=16)
parser.add_argument("--train_epochs", type=int, default=100)
parser.add_argument("--model", default="DeepSet")

def train(model, train_data, val_data, train_size, val_size, optimizer, scheduler, criterion, train_epochs, input_size, batch_size) :

    acc_train = []
    acc_val = []
    loss_train = []
    loss_val = []

    best_val_acc = 0.0

    for epoch in range(train_epochs):
        model.train()

        train_data_iterator = data_loader.data_iterator(train_data, input_size, batch_size, shuffle=True)

        losses, total, correct = [], 0, 0
        num_steps = (train_size + 1) // batch_size
        t = trange(num_steps)
        for i in t:
            train_batch, labels_batch, sizes_batch = next(train_data_iterator)

            sets = torch.Tensor(train_batch)
            lbls = torch.Tensor(labels_batch)
            preds = model(sets)
            loss = criterion(preds, lbls)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            losses.append(loss.item())
            total += lbls.shape[0]
            correct += (torch.round(preds) == lbls).sum().item()

        avg_loss, avg_acc = np.mean(losses), correct / total
        acc_train.append(avg_acc)
        loss_train.append(avg_loss)
        print(f"Epoch {epoch}: train loss {avg_loss:.3f} train acc {avg_acc:.3f}")

        if epoch % 1 == 0:
            _, _, avg_loss, avg_acc = evaluate(model, val_data, input_size, batch_size, val_size)
            acc_val.append(avg_acc)
            loss_val.append(avg_loss)
            print(f"Epoch {epoch}: val loss {avg_loss:.3f} val acc {avg_acc:.3f}")

            is_best = avg_acc >= best_val_acc

            scheduler.step(avg_acc)

        #    Save weights
            utils.save_checkpoint({'epoch': epoch + 1, 'state_dict': model.state_dict(),
                               'optim_dict': optimizer.state_dict()}, is_best=is_best, checkpoint="checkpoints")

            # If best_eval, best_save_path
            if is_best:
                best_val_acc = avg_acc

    History = {
    "acc_train" : acc_train,
    "acc_val" : acc_val,
    "loss_train" : loss_train,
    "loss_val" : loss_val
    }

    return History

def evaluate(model, test_data, input_size, batch_size, test_size) :
    test_data_iterator = data_loader.data_iterator(test_data, input_size, batch_size, shuffle=True)

    outputs = []
    labels = []

    model.eval()
    losses, total, correct = [], 0, 0
    num_steps = (test_size + 1) // batch_size
    t = trange(num_steps)
    for i in t:
        train_batch, labels_batch, sizes_batch = next(test_data_iterator)
        sets = torch.Tensor(train_batch)
        lbls = torch.Tensor(labels_batch)
        preds = model(sets)
        loss = criterion(preds, lbls)

        outputs += torch.round(preds).tolist()
        labels += lbls.tolist()

        losses.append(loss.item())
        total += lbls.shape[0]
        correct += (torch.round(preds) == lbls).sum().item()
    avg_loss, avg_acc = np.mean(losses), correct / total
    print(f"test loss {avg_loss:.3f} test acc {avg_acc:.3f}")

    return outputs, labels, avg_loss, avg_acc


if __name__ == '__main__':

    args = parser.parse_args()
    args.exp_name = f"N{args.num_pts}_{args.model}_d{args.dim}h{args.n_heads}i{args.n_anc}_lr{args.learning_rate}bs{args.batch_size}"
    log_dir = "result/" + args.exp_name
    model_path = log_dir + "/model"

    # use GPU if available
    cuda = torch.cuda.is_available()

    # Set the random seed for reproducible experiments
    torch.manual_seed(230)
    if cuda:
        torch.cuda.manual_seed(230)

    # load data
    dir_data = "./supervised_data/dentaire_clause"
    #dir_data = "./data"

    data_loader = DataLoader(dir_data)
    data = data_loader.load_data(['train', 'val'], dir_data)
    train_data = data['train']
    val_data = data['val']

    # specify the train and val dataset sizes
    train_size = train_data['size']
    val_size = val_data['size']

    train_epochs = args.train_epochs
    batch_size = args.batch_size
    #input_size = 136
    input_size = 158
    #input_size = 3

    if args.model == "DeepSet" :
        model = DeepSet(dim_input=input_size, num_outputs=1, dim_output=1, pool="mean")
    elif args.model == "SetTransformer_dropout":
        model = SetTransformer_dropout(dim_hidden=args.dim, num_heads=args.n_heads, num_inds=args.n_anc)
    elif args.model == "SetTransformer":
        model = SetTransformer(dim_input=input_size, num_outputs=1, dim_output=1, num_inds=32, dim_hidden=args.dim, num_heads=4, ln=False)

    weights_init_kaiming_uniform(model)

    if cuda:
        model = model.cuda

    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate,weight_decay=0.1)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', factor=0.5, patience=3, verbose=True)
    criterion = nn.BCELoss()

    History = train(model, train_data, val_data, train_size, val_size, optimizer, scheduler, criterion, train_epochs, input_size, batch_size)

    plot(History)

    # Reload weights from the saved file
    utils.load_checkpoint(os.path.join("checkpoints", "best" + '.pth.tar'), model)

    print("=============== Scores train data =================")

    outputs, labels, avg_loss, avg_acc = evaluate(model, train_data, input_size, batch_size, train_size)

    score(outputs, labels)

    print("=============== Scores test data =================")

    data = data_loader.load_data(['test'], dir_data)
    test_data = data['test']
    test_size = test_data['size']

    outputs, labels, avg_loss, avg_acc = evaluate(model, test_data, input_size, batch_size, test_size)

    score(outputs, labels)




