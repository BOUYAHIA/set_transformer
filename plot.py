import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score


def plot(History) :

    acc_train = History["acc_train"]
    acc_val = History["acc_val"]
    loss_train = History["loss_train"]
    loss_val = History["loss_val"]

    plt.subplot(211)
    plt.plot(acc_train, label="acc_train")
    plt.plot(acc_val, label="acc_val")
    plt.legend()
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")

    plt.subplot(212)
    plt.plot(loss_train, label="loss_train")
    plt.plot(loss_val, label="loss_val")
    plt.legend()
    plt.xlabel("Epoch")
    plt.ylabel("Loss")

    plt.show()

def score(outputs, labels) :
    outputs = [round(x) for x in outputs ]
    labels = [round(x) for x in labels]
    print(confusion_matrix(labels, outputs))
    print(accuracy_score(labels, outputs))
    print(recall_score(labels, outputs))
    print(precision_score(labels, outputs))
