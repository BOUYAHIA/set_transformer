import random
import numpy as np
import os
import sys
import ast

import torch
from torch.autograd import Variable
from torch import FloatTensor
from torch.utils.data.dataset import Dataset
from typing import Tuple
from itertools import repeat

class DataLoader(Dataset):
    """
    Handles all aspects of the data. Stores the dataset_params, vocabulary and tags with their mappings to indices.
    """
    def __init__(self, data_dir):

        # loading dataset_params
        json_path = os.path.join(data_dir, 'dataset_params.json')

    def load_sets_labels(self, sets_file, labels_file, d):

        sets = []
        labels = []

        with open(sets_file) as f:
            for i, line in enumerate(f):
                liste = [ast.literal_eval(x) for x in line.strip().split(';')]
                sets.append(liste)

        with open(labels_file) as f:
            for i, line in enumerate(f):
                labels.append(int(line))

                # checks to ensure there is a tag for each token
        assert len(labels) == len(sets)

        # storing sentences and labels in dict d
        d['data'] = sets
        d['labels'] = labels
        d['size'] = len(sets)

    def load_data(self, types, data_dir):

        data = {}

        for split in ['train', 'val', 'test']:
            if split in types:
                sets_file = os.path.join(data_dir, split, "sets.txt")
                labels_file = os.path.join(data_dir, split, "labels.txt")
                data[split] = {}
                self.load_sets_labels(sets_file, labels_file, data[split])

        return data

    def data_iterator(self, data, input_size, batch_size, shuffle=False):

        # make a list that decides the order in which we go over the data- this avoids explicit shuffling of data
        order = list(range(data['size']))
        if shuffle:
            random.seed(230)
            random.shuffle(order)

        # one pass over data
        for i in range((data['size'] + 1) // batch_size):
            # fetch sentences and tags
            batch_sets = [data['data'][idx] for idx in order[i * batch_size:(i + 1) * batch_size]]
            batch_labels = [data['labels'][idx] for idx in order[i * batch_size:(i + 1) * batch_size]]
            batch_sizes = [len(data['data'][idx]) for idx in order[i * batch_size:(i + 1) * batch_size]]

            # since all data are indices, we convert them to torch LongTensors
            print(i)
            batch_max_len = max([len(s) for s in batch_sets])

            listofzeros = list()
            for i in range(input_size):
                listofzeros.append(0.)

            for i in range(len(batch_sets)):
                if len(batch_sets[i]) < batch_max_len:
                    for j in range(batch_max_len - len(batch_sets[i])):
                        batch_sets[i].append(listofzeros)

            batch_sets, batch_labels = torch.FloatTensor(batch_sets), torch.FloatTensor(batch_labels)

            # shift tensors to GPU if available
            if torch.cuda.is_available():
                batch_sets, batch_labels = batch_sets.cuda(), batch_labels.cuda()

            # convert them to Variables to record operations in the computational graph
            batch_sets, batch_labels = Variable(batch_sets), Variable(batch_labels)

            yield batch_sets, batch_labels, batch_sizes
