from modules import *

class DeepSet(nn.Module):
    def __init__(self, dim_input, num_outputs, dim_output, pool="mean", dim_hidden=256):
        super(DeepSet, self).__init__()
        self.num_outputs = num_outputs
        self.dim_output = dim_output
        self.pool = pool
        self.alpha = nn.Parameter(torch.tensor(100.0, requires_grad=True))
        self.enc = nn.Sequential(
                nn.Linear(dim_input, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, 100))
        self.dec = nn.Sequential(
                nn.Linear(100, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_hidden),
                nn.LayerNorm([dim_hidden]),
                nn.ReLU(),
                nn.Linear(dim_hidden, dim_output),
                nn.Sigmoid())

    def forward(self, X):
        X = self.enc(X)
        if self.pool == "max":
            X = X.max(-2)[0]
        elif self.pool == "mean":
            X = X.mean(-2)
        elif self.pool == "sum":
            X = X.sum(-2)
        elif self.pool == "adaset":
            X = (1 / (self.alpha + 0.0001)) * torch.log(((self.alpha + 0.0001) * torch.exp(X)).mean(dim=1) + 0.0001)

        X = self.dec(X).reshape(-1, self.num_outputs, self.dim_output)
        return X.squeeze()

class SetTransformer(nn.Module):
    def __init__(self, dim_input, num_outputs, dim_output,
            num_inds=32, dim_hidden=128, num_heads=4, ln=False):
        super(SetTransformer, self).__init__()
        self.enc = nn.Sequential(
                ISAB(dim_input, dim_hidden, num_heads, num_inds, ln=ln),
                ISAB(dim_hidden, dim_hidden, num_heads, num_inds, ln=ln))
        self.dec = nn.Sequential(
                PMA(dim_hidden, num_heads, num_outputs, ln=ln),
                SAB(dim_hidden, dim_hidden, num_heads, ln=ln),
                SAB(dim_hidden, dim_hidden, num_heads, ln=ln),
                nn.Linear(dim_hidden, dim_output),
                nn.Sigmoid())

    def forward(self, X):
        return self.dec(self.enc(X)).squeeze()

class SetTransformer_dropout(nn.Module):
    def __init__(self, dim_input=3, num_outputs=1, dim_output=1, num_inds=32,
            dim_hidden=128, num_heads=4, ln=False, ):
        super(SetTransformer_dropout, self).__init__()
        self.enc = nn.Sequential(
            ISAB(dim_input, dim_hidden, num_heads, num_inds, ln=ln),
            ISAB(dim_hidden, dim_hidden, num_heads, num_inds, ln=ln),
        )
        self.dec = nn.Sequential(
            nn.Dropout(),
            PMA(dim_hidden, num_heads, num_outputs, ln=ln),
            nn.Dropout(),
            nn.Linear(dim_hidden, dim_output),
            nn.Sigmoid()
        )

    def forward(self, X):
        return self.dec(self.enc(X)).squeeze()


def weights_init_kaiming_uniform(net, biais = True):
    for module in net.modules():
        if isinstance(module, nn.Linear):
            torch.nn.init.kaiming_uniform_(module.weight, 1.0)
            if biais :
                torch.nn.init.zeros_(module.bias)
