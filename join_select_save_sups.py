# -*- coding: utf-8 -*-
import csv
import os
import pandas as pd

path_save = '/home/guests/abdelali.bouyahia/Livraison_Fraude_Cert_202106031304.csv'
path_cert = "/data/UL/transfert/Prevention_de_la_fraude/Livraison_fraude_20210607/Livraison_Fraude_Cert_202106031304.csv"
path_fourni = '/data/UL/transfert/Prevention_de_la_fraude/Livraison_fraude_20210607/Livraison_Fraude_Fourn_202106031549.csv'
path_contra = '/data/UL/transfert/Prevention_de_la_fraude/Livraison_fraude_20210212/DonneesContractuelles.csv'

assert os.path.isfile(path_cert), "No csv file found at {}".format(path_cert)
assert os.path.isfile(path_fourni), "No csv file found at {}".format(path_fourni)
assert os.path.isfile(path_contra), "No csv file found at {}".format(path_contra)

df_contr = pd.read_csv(path_contra, error_bad_lines=False, sep=';', encoding='cp1252')
df_cert = pd.read_csv(path_cert, error_bad_lines=False, sep=';', encoding='cp1252')

df = pd.merge(df_cert, df_contr,  how='left', left_on=['A_NO_CAD', 'A_NO_HIST_CAD', 'A_NO_CL_GAR'], right_on = ['A_NO_CAD', 'A_NO_HIST_CAD', 'A_NO_CL_GAR'])

df= df[df['IS_SUSPECT'] == 'O']

if os.path.exists(path_save):
  os.remove(path_save)

df.to_csv(path_save ,encoding='cp1252',  sep=';', index=False)

assert os.path.isfile(path_save), "No csv file found at {}".format(path_save)