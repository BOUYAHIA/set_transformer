# -*- coding: utf-8 -*-
import csv
import os
import pandas as pd
from datetime import datetime
from statistics import *
import argparse
import logging
import utils

parser = argparse.ArgumentParser()
# path_certificat = '/data/UL/transfert/Prevention_de_la_fraude/GSS_FILE_CERTIFICATS_20190610.csv'
path_certi = '/home/guests/abdelali.bouyahia/sample_01102020.csv'
path_fourni = '/data/UL/transfert/Prevention_de_la_fraude/GSS_FILE_FOURNISSEURS_20190610.csv'
path_contra = '/data/UL/transfert/Prevention_de_la_fraude/Livraison_fraude_20210212/DonneesContractuelles.csv'
save_dir = 'supervised_data'
CATG_RECLAM = 'autre_clause'

parser.add_argument("path_certi", default=path_certi)
parser.add_argument("path_fourni", default=path_fourni)
parser.add_argument("path_contra", default=path_contra)
parser.add_argument("save_dir", default=save_dir)
parser.add_argument("CATG_RECLAM", default=CATG_RECLAM)

class Load_process_save_data:
    """
    Classe LoadData qui va se charger de :

        - charger les fichiers CSVs GSS_FILE_CERTIFICATS_20190610.csv et GSS_FILE_FOURNISSEURS_20190610.csv et
        - Actuellement j'utilise seulement les formulaires labelés donc j'utilise le fichier sample_01102020.csv
             au lieu de GSS_FILE_FOURNISSEURS_20190610.csv
        - extraire les features
        - encoder les variables catégorielles en utilisant le one-hote-vector
        - enregistrer chaque formulaire dans une ligne et les mettre dans un fichier sets.csv dans un répertoire sous la forme :

             A_ID_FORM_RECLAM, formulaire, IS_SUSPECT, IS_AUDITE
             FORM-006241725;[[0,1...., 1], [0,1...., 0], [0,0...., 1]];O;0
             FORM-006241725;[[1,1...., 1], [0,0...., 1]];O;1

        - enregistrer chaque label dans une ligne et les mettre dans un fichier labels.csv dans un répertoire sous la forme
            0
            1

        Args :

            path_cert : chemin du fichier qui contient les données des certificats
            path_fourn : chemin du fichier qui contient les données des fournisseurs
            path_contra : chemin du fichier qui contient les données contractuelles
        """

    def __init__(self, path_certificat, path_fournisseur, path_contractuel, CATG_RECLAM):
        self.path_certificat = path_certificat
        self.path_fournisseur = path_fournisseur
        self.path_contractuel = path_contractuel
        self.CATG_RECLAM = CATG_RECLAM

    def load_process_data(self):
        """Loads dataset into memory from csv file"""
        msg = "{} file not found. Make sure you have the right dataset in this path".format(path_certificat)
        assert os.path.isfile(path_certificat), msg
        msg = "{} file not found. Make sure you have the right dataset in this path".format(path_fournisseur)
        assert os.path.isfile(path_fournisseur), msg
        msg = "{} file not found. Make sure you have the right dataset in this path".format(path_contractuel)
        assert os.path.isfile(path_contractuel), msg

        self.df_certi = pd.read_csv(self.path_certificat, error_bad_lines=False, sep=';', encoding='cp1252')
        self.df_fourni = pd.read_csv(self.path_fournisseur, error_bad_lines=False, sep=';', encoding='cp1252')

        logging.info('Taille du fichier des certificats est : {} \n'.format(self.df_certi.shape))
        logging.info('Taille du fichier des fournisseurs est : {} \n'.format(self.df_fourni.shape))

        logging.info('Choix des variables et lignes ... \n')
        self.choose_var_and_rows()

        logging.info('Taille du fichier certificats après le choix de lignes et de colonnes : {} \n'.format(self.df_certi.shape))
        logging.info('Les feautures choisis sont : \n')
        logging.info(str(list(self.df_certi.columns))+'\n')

        logging.info('Imputation de données manquantes ...\n')
        self.fill_nan()

        logging.info('Ajout des informations sur le fournisseurs ...\n')
        self.add_fourni_features()

        logging.info('Taille du fichier certificats après l\'ajout des informations des fournisseurs: {} \n'.format(self.df_certi.shape))
        logging.info('Les feautures après transormations sont \n')
        logging.info(str(list(self.df_certi.columns))+'\n')

        logging.info('Ingénieurie des caractéristiques ... \n')
        self.transform_var()

        logging.info('Taille du fichier après transformations : {} \n'.format(self.df_certi.shape))
        logging.info('Les feautures après transormations sont \n')
        logging.info(str(list(self.df_certi.columns))+'\n')

        logging.info('Faire le one_hot_encoding ...\n')
        self.get_one_hot_encoding()

        logging.info('Taille du fichier après le one hot encoding : {} \n'.format(self.df_certi.shape))
        logging.info('Les feautures après le one hot encoding \n')
        logging.info(str(list(self.df_certi.columns))+'\n')

        logging.info('Extraction des ensembles et de leurs étiquettes ...\n')
        self.get_set_data_label()

        self.nb_formulaire = self.df_certi['A_ID_FORM_RECLAM'].nunique()

        assert len(self.IS_AUDITE)== self.nb_formulaire, "La taille de IS_AUDITE n'est pas égale au nombre de formulaires"
        assert len(self.IS_SUSPECT) == self.nb_formulaire, "La taille de IS_SUSPECT n'est pas égale au nombre de formulaires"
        assert len(self.data)== self.nb_formulaire, "La taille de data n'est pas égale au nombre de formulaires"
        assert len(self.A_ID_FORM_RECLAM) == self.nb_formulaire, "La taille de A_ID_FORM_RECLAM n'est pas égale au nombre de formulaires"

        logging.info('Le nombre de formulaires extraits est : {} \n'.format(self.nb_formulaire))

        taille = [len(x) for x in self.data]

        logging.info('La taille du formulaire le plus long est : {} \n'.format(max(taille)))
        logging.info('La taille du formulaire le plus petit est : {} \n'.format(min(taille)))
        logging.info('La taille moyenne des formulaires est : {} \n'.format(mean(taille)))
        logging.info('Le nombre de formulaires audités est : {} \n'.format(self.IS_AUDITE.count(1)))
        logging.info('Le pourcentage de formulaires autités est : {} %\n'.format((self.IS_AUDITE.count(1)/len(self.IS_AUDITE))*100))

        logging.info('les features finales sont \n')
        logging.info(self.columns +'\n')

        # return self.A_ID_FORM_RECLAM, self.data, self.IS_SUSPECT, self.IS_AUDITE

    def choose_var_and_rows(self):
        """
        Choisir les réclamations :
            - c'est la première transaction : réclamation soumise par le client
            - enlever les réclamations qui ont un montant déclaré négatif
            - enlever les réclamations qui n'ont pas une date de réception exacte

        Choisir les features jugés pertinents pour la prédiction de la fraude
        """
        self.df_certi = self.df_certi[self.df_certi['I_TRANS'] == 1]
        self.df_certi = self.df_certi[self.df_certi['M_MT_RECLAME'] > 0]
        self.df_certi = self.df_certi[self.df_certi.DATE_DE_RECEPTION != 99991231]

        self.df_certi = self.df_certi[['A_NO_CERT', 'A_ID_FORM_RECLAM', 'D_ID_FOURN','AGE_ADH', 'ASSU_RECLAM', 'CATG_AUTR_PAYEUR',
                 'CLAUSE', 'DE_CD_CATG_CL', 'DE_TYPE_FOURN', 'D_IND_DOUB_ASSU', 'D_SEXE_ADH', 'M_MT_AUTR_PAYEUR', 'M_MT_RECLAME', 'CATG_RECLAM',
                 'TYPE_BEN_PAIE', 'TYPE_RECLAM', 'UNITE_STRUCTURELLE','DATE_DE_SERVICE', 'DATE_DE_RECEPTION', 'IS_SUSPECT', 'IS_AUDITE']]
        # 'MODE_TRANS_RECLAM', 'SOUR_SOUM_RECLAM', 'D_IND_SYNDICAT', 'DE_CD_REGR1_CL', 'DE_CD_REGR_CATG',

        if self.CATG_RECLAM == 'autre_clause' :
            self.df_certi = self.df_certi[self.df_certi.CATG_RECLAM == 'Autres clauses']
            self.df_certi.drop(['CATG_RECLAM'], axis=1, inplace=True)
        elif self.CATG_RECLAM == 'soin_dentaire' :
            self.df_certi = self.df_certi[self.df_certi.CATG_RECLAM == 'Soins dentaires']
            self.df_certi.drop(['CATG_RECLAM'], axis=1, inplace=True)

    def fill_nan(self):
        """
        remplir les valeurs manquantes pour certaines features
        """
        self.df_certi.fillna(value={"AGE_ADH": self.df_certi['AGE_ADH'].mean()}, inplace=True)
        self.df_certi.fillna(value={"DE_TYPE_FOURN": "NOT_DEFINED"}, inplace=True)
        self.df_certi.fillna(value={"UNITE_STRUCTURELLE": "NOT_DEFINED"}, inplace=True)
        # self.df.fillna(value={"DE_CD_REGR1_CL": "NOT_DEFINED"}, inplace=True)

    def add_fourni_features(self) :
        """
        Ajouter des features des fournisseurs : Pour chaque fournisseur et chaque service (CLAUSE) que fourni ce fournisseur on calcule les
            features suivants :
            - Montant maximal déclaré
            - Montant minimal déclaré
            - Moyenne des montants déclarés
            - Mediane des montants déclarés
            - Ecart-type des montants déclarés
        """
        self.df_fourni = self.df_fourni[self.df_fourni['I_TRANS'] == 1]
        self.df_fourni = self.df_fourni[self.df_fourni['M_MT_RECLAME'] > 0]
        self.df_fourni = self.df_fourni[self.df_fourni.DATE_DE_RECEPTION != 99991231]

        self.df_fourni = self.df_fourni[['CLAUSE', 'D_ID_FOURN', 'DE_TYPE_FOURN', 'M_MT_RECLAME', 'IS_SUSPECT']]
        df_suspect = self.df_fourni[self.df_fourni['IS_SUSPECT'] == 'O']
        df_no_suspect = self.df_fourni[self.df_fourni['IS_SUSPECT'] == 'N']
        print('shape four no suspect : ', df_no_suspect.shape)

        gb_suspect = df_suspect.groupby(['D_ID_FOURN', 'CLAUSE'])
        gb_no_suspect = df_no_suspect.groupby(['D_ID_FOURN', 'CLAUSE'])

        montant_recl_suspect_clause_max = gb_suspect['M_MT_RECLAME'].max()
        montant_recl_suspect_clause_min = gb_suspect['M_MT_RECLAME'].min()
        montant_recl_suspect_clause_mean = gb_suspect['M_MT_RECLAME'].mean()
        montant_recl_suspect_clause_median = gb_suspect['M_MT_RECLAME'].median()
        montant_recl_suspect_clause_std = gb_suspect['M_MT_RECLAME'].std()

        montant_recl_no_suspect_clause_max = gb_no_suspect['M_MT_RECLAME'].max()
        montant_recl_no_suspect_clause_min = gb_no_suspect['M_MT_RECLAME'].min()
        montant_recl_no_suspect_clause_mean = gb_no_suspect['M_MT_RECLAME'].mean()
        montant_recl_no_suspect_clause_median = gb_no_suspect['M_MT_RECLAME'].median()
        montant_recl_no_suspect_clause_std = gb_no_suspect['M_MT_RECLAME'].std()

        groupped_suspect = pd.concat([montant_recl_suspect_clause_max, montant_recl_suspect_clause_min,
                                      montant_recl_suspect_clause_mean, montant_recl_suspect_clause_median,
                                      montant_recl_suspect_clause_std], axis=1)
        groupped_no_suspect = pd.concat([montant_recl_no_suspect_clause_max, montant_recl_no_suspect_clause_min,
                                         montant_recl_no_suspect_clause_mean, montant_recl_no_suspect_clause_median,
                                         montant_recl_no_suspect_clause_std], axis=1)

        # Il existe des ID de fournisseurs qui se trouve dans le fichier certificats mais qui ne se trouve pas dans le fichier des fournisseurs.
        # Donc, on ne peut pas récuperer les informations sur tous les fournisseurs en utilsant juste le fichier des fournisseurs
        # La solution est de récuperer ces informations manquantes a partir du fichier de certificats

        df_certi_tempo = self.df_certi[['CLAUSE', 'D_ID_FOURN', 'DE_TYPE_FOURN', 'M_MT_RECLAME', 'IS_SUSPECT']]
        df_suspect_certi = df_certi_tempo[df_certi_tempo['IS_SUSPECT'] == 'O']
        print('shape pour suspect : ', df_suspect_certi.shape)
        df_no_suspect_certi = df_certi_tempo[df_certi_tempo['IS_SUSPECT'] == 'N']
        print('shape pour no suspect : ', df_no_suspect_certi.shape)

        gb_suspect_certi = df_suspect_certi.groupby(['D_ID_FOURN', 'CLAUSE'])
        gb_no_suspect_certi = df_no_suspect_certi.groupby(['D_ID_FOURN', 'CLAUSE'])

        montant_recl_suspect_clause_max_certi = gb_suspect_certi['M_MT_RECLAME'].max()
        montant_recl_suspect_clause_min_certi = gb_suspect_certi['M_MT_RECLAME'].min()
        montant_recl_suspect_clause_mean_certi = gb_suspect_certi['M_MT_RECLAME'].mean()
        montant_recl_suspect_clause_median_certi = gb_suspect_certi['M_MT_RECLAME'].median()
        montant_recl_suspect_clause_std_certi = gb_suspect_certi['M_MT_RECLAME'].std()

        montant_recl_no_suspect_clause_max_certi = gb_no_suspect_certi['M_MT_RECLAME'].max()
        montant_recl_no_suspect_clause_min_certi = gb_no_suspect_certi['M_MT_RECLAME'].min()
        montant_recl_no_suspect_clause_mean_certi = gb_no_suspect_certi['M_MT_RECLAME'].mean()
        montant_recl_no_suspect_clause_median_certi = gb_no_suspect_certi['M_MT_RECLAME'].median()
        montant_recl_no_suspect_clause_std_certi = gb_no_suspect_certi['M_MT_RECLAME'].std()

        groupped_suspect_certi = pd.concat([montant_recl_suspect_clause_max_certi, montant_recl_suspect_clause_min_certi,
                                      montant_recl_suspect_clause_mean_certi, montant_recl_suspect_clause_median_certi,
                                      montant_recl_suspect_clause_std_certi], axis=1)
        groupped_no_suspect_certi = pd.concat([montant_recl_no_suspect_clause_max_certi, montant_recl_no_suspect_clause_min_certi,
                                         montant_recl_no_suspect_clause_mean_certi, montant_recl_no_suspect_clause_median_certi,
                                         montant_recl_no_suspect_clause_std_certi], axis=1)


        self.df_certi.reset_index(drop=True, inplace=True)
        list_max = list()
        list_min = list()
        list_mean = list()
        list_median = list()
        list_std = list()

        for i in range(0, self.df_certi.shape[0]):
            try:

                list_max.append(groupped_no_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][0])
                list_min.append(groupped_no_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][1])
                list_mean.append(groupped_no_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][2])
                list_median.append(groupped_no_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][3])
                list_std.append(groupped_no_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][4])
            except:
                try :
                    list_max.append(groupped_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][0])
                    list_min.append(groupped_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][1])
                    list_mean.append(groupped_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][2])
                    list_median.append(groupped_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][3])
                    list_std.append(groupped_suspect.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][4])
                except :
                    try :
                        list_max.append(groupped_no_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][0])
                        list_min.append(groupped_no_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][1])
                        list_mean.append(groupped_no_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][2])
                        list_median.append(groupped_no_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][3])
                        list_std.append(groupped_no_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][4])
                    except:
                        list_max.append(groupped_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][0])
                        list_min.append(groupped_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][1])
                        list_mean.append(groupped_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][2])
                        list_median.append(groupped_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][3])
                        list_std.append(groupped_suspect_certi.loc[(self.df_certi['D_ID_FOURN'][i], self.df_certi['CLAUSE'][i])][4])

        size = self.df_certi.shape[0]
        assert len(list_max) == size, "la taille de list_max doit égale au nombre d'exemples dans self.df_certi"
        assert len(list_min) == size, "la taille de list_min doit égale au nombre d'exemples dans self.df_certi"
        assert len(list_mean) == size, "la taille de list_mean doit égale au nombre d'exemples dans self.df_certi"
        assert len(list_median) == size, "la taille de list_median doit égale au nombre d'exemples dans self.df_certi"
        assert len(list_std) == size, "la taille de list_std doit égale au nombre d'exemples dans self.df_certi"

        self.df_certi['M_MAX_FOURN_RECLAME'] = list_max
        self.df_certi['M_MIN_FOURN_RECLAME'] = list_min
        self.df_certi['M_MEAN_FOURN_RECLAME'] = list_mean
        self.df_certi['M_MEDIAN_FOURN_RECLAME'] = list_median
        #self.df_certi['M_STD_FOURN_RECLAME'] = list_std


    def transform_var(self):
        """
        Faire des transformations sur certaines features et en extraire d'autres variables utiles
        pour la détection de la fraude comme:

            - La durée entre la date de service et la date de réception en jour
            - La date de service et de réception appartiennent elles a la même année
            ...
        """

        self.df_certi['DATE_DE_SERVICE'] = pd.to_datetime(self.df_certi['DATE_DE_SERVICE'].astype(str), format='%Y%m%d')
        self.df_certi['DATE_DE_RECEPTION'] = pd.to_datetime(self.df_certi['DATE_DE_RECEPTION'].astype(str), format='%Y%m%d')
        self.df_certi['DATE_RECEP_MOINS_SERV'] = self.df_certi['DATE_DE_RECEPTION'] - self.df_certi['DATE_DE_SERVICE']
        self.df_certi['DATE_RECEP_MOINS_SERV'] = self.df_certi['DATE_RECEP_MOINS_SERV'].dt.days.astype('int16')

        self.df_certi['SAME_YEAR'] = 0
        self.df_certi.loc[self.df_certi['DATE_DE_SERVICE'].dt.year == self.df_certi['DATE_DE_RECEPTION'].dt.year, 'SAME_YEAR'] = 1

        # j'ai utilise datetime(x.year-1, 12, 31)  pour evite la division par 0
        self.df_certi['serv'] = [datetime(x.year + 1, 1, 1) - x for x in self.df_certi['DATE_DE_SERVICE']]
        self.df_certi['recep'] = [x - datetime(x.year - 1, 12, 31) for x in self.df_certi['DATE_DE_RECEPTION']]
        self.df_certi['RATIO_RECEP_SERV '] = self.df_certi['serv'] / self.df_certi['recep']
        self.df_certi['serv'] = self.df_certi['serv'].dt.days.astype('int16')
        self.df_certi['recep'] = self.df_certi['recep'].dt.days.astype('int16')

        self.df_certi.IS_AUDITE.replace(('O', 'N'), (1, 0), inplace=True)

    def get_one_hot_encoding(self):
        """
        One-hot-vector pour les variables catégorielles
        """
        cols = ['ASSU_RECLAM', 'CATG_AUTR_PAYEUR','CLAUSE', 'DE_CD_CATG_CL', 'DE_TYPE_FOURN',
                'D_IND_DOUB_ASSU', 'D_SEXE_ADH','TYPE_BEN_PAIE', 'TYPE_RECLAM', 'UNITE_STRUCTURELLE']
        # 'MODE_TRANS_RECLAM', 'SOUR_SOUM_RECLAM', 'D_IND_SYNDICAT', 'DE_CD_REGR1_CL', 'DE_CD_REGR_CATG',

        if self.CATG_RECLAM != 'autre_clause' and self.CATG_RECLAM != 'soin_dentaire' :
            cols.append('CATG_RECLAM')

        i = 0
        for col in cols:
            self.df_certi[col] = pd.Categorical(self.df_certi[col])
            if i==0 :
                dataDummies = pd.get_dummies(self.df_certi[col], prefix=col)
            else :
                dataDummies = pd.concat([dataDummies, pd.get_dummies(self.df_certi[col], prefix=col)], axis=1)
            i += 1

        self.df_certi = pd.concat([self.df_certi, dataDummies], axis=1)
        self.df_certi.drop(cols, axis=1, inplace=True)

    def get_set_data_label(self) :
        """
        - Construire les sets a partir des réclamations
        - Supprimer les variables a utilisté technique
        """
        self.IS_AUDITE = []
        self.data = []
        self.IS_SUSPECT = []
        self.A_ID_FORM_RECLAM = []
        cols = ['D_ID_FOURN', 'A_NO_CERT', 'A_ID_FORM_RECLAM', 'DATE_DE_SERVICE', 'DATE_DE_RECEPTION', 'IS_SUSPECT', 'IS_AUDITE']

        #print(self.df_certi.columns[self.df_certi.isnull().any()].tolist())

        by_formulaire = self.df_certi.groupby('A_ID_FORM_RECLAM')

        i = 0
        for formulaire, frame in by_formulaire:
            self.A_ID_FORM_RECLAM.append(frame.iloc[0]['A_ID_FORM_RECLAM'])
            self.IS_AUDITE.append(frame.iloc[0]['IS_AUDITE'])
            self.IS_SUSPECT.append(frame.iloc[0]['IS_SUSPECT'])
            self.data.append(frame.drop(cols,axis=1).values.tolist())

            if i == 0:
                self.columns = str(list(frame.drop(cols,axis=1).columns))
            i += 1

    def save_dataset(self, save_dir):
        """
        Enregister les sets dans un fichier nommé data.csv dans le répertoire save_dir sous la forme

             A_ID_FORM_RECLAM, formulaire, IS_SUSPECT, IS_AUDITE
             FORM-006241725;[[0,1...., 1], [0,1...., 0], [0,0...., 1]];O;0
             FORM-006241725;[[1,1...., 1], [0,0...., 1]];O;1
        Args:
            save_dir: (string)
        """
        #print("Saving in {}...".format(save_dir))

        # Create directory if it doesn't exist
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        with open(os.path.join(save_dir, 'data.csv'), 'w') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter =';')
            list_to_write = ['A_ID_FORM_RECLAM', 'data', 'IS_SUSPECT', 'IS_AUDITE']
            csvwriter.writerow(list_to_write)

            for i in range(self.nb_formulaire):
                # creating a csv writer object
                list_to_write = [self.A_ID_FORM_RECLAM[i], self.data[i], self.IS_SUSPECT[i], self.IS_AUDITE[i]]

                # writing the fields
                csvwriter.writerow(list_to_write)


if __name__ == '__main__':

    args = parser.parse_args()

    path_certificat = args.path_certi
    path_fournisseur = args.path_fourni
    path_contractuel = args.path_contra
    CATG_RECLAM =  args.CATG_RECLAM

    assert os.path.isfile(path_certificat), "No csv file found at {}".format(path_certificat)
    assert os.path.isfile(path_fournisseur), "No csv file found at {}".format(path_fournisseur)
    assert os.path.isfile(path_contractuel), "No csv file found at {}".format(path_contractuel)

    # Create directory if it doesn't exist
    if not os.path.exists(args.save_dir):
        os.makedirs(args.save_dir)
    # Set the logger
    utils.set_logger(os.path.join(args.save_dir, 'build_data.log'))

    logging.info("Loading dataset into memory, process it and save it into at {} ...".format(args.save_dir))
    load_process_save_data = Load_process_save_data(path_certificat, path_fournisseur, path_contractuel, CATG_RECLAM)
    load_process_save_data.load_process_data()

    load_process_save_data.save_dataset(os.path.join(args.save_dir, args.CATG_RECLAM))

    logging.info("- done.")




    


